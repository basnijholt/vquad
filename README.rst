Vquad
=====

Vquad is a work-in-progress towards a general-purpose, robust, efficient, and
parallel library for numerical integration.

History
-------

Vquad is based on algorithm 4 from the article "Increasing the Reliability of
Adaptive Quadrature Using Explicit Interpolants", Pedro Gonnet, ACM TOMS 37,
26 (2010).

Usage example
-------------

```
import numpy
import vquad

# Simple usage.
igral, err = vquad.vquad(numpy.cos, -1, 1, rtol=1e-10)

# Object interface.
it = vquad.Vquad(numpy.cos, -1, 1)
igral, err = it.improve_until(rtol=1e-10)

# Evaluate interpolant.
xs = numpy.linspace(-1, 1, 101)
ys = it(xs)
```

Benachmarks and tests
---------------------

Vquad includes extensive tests and benchmarks.  The tests focus on verifying
correctness and can be run with
```
python3 -m pytest -s
```
The `-s` shows some statistics that are gathered during the testing.

The benchmarks take longer to run and allow to quantitatively compare the
performance of the algorithm.  The benchmark is run by executing the vquad
module.  To get help, run:
```
python3 -m vquad -h
```
